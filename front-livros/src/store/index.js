import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    livros: [],
  },
  getters:{
    qtdLivros(state){
      return state.livros.length
    },
    livros(state){
      return state.livros
    }
  },
  mutations: {
    addLivros(state, livros){
      state.livros = livros
    }
  },
  actions: {
    getLivros(context){
      // console.log(Vue.axios)
      let livro = []
      Vue.axios.get(`/books`).then(resp => {
        resp.data.map((obj) => {
          livro.push({...obj})
        })
      })
      context.commit("addLivros", livro)
    },
  }
})
